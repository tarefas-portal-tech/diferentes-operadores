# Diferentes Operadores

var
nota: inteiro
faltas: inteiro
convidados: inteiro
fumante: booleana
dia: texto
feriado: booleana

inicio

sentença (“nota <= 10”);
resultado (verdadeiro);
sentença (“(nota <= 6) and (faltas <= 3)”);
resultado (falso);
sentença (“(convidados > 4) or (fumante == true)”);
resultado (falso);
sentença (“(dia == “sab”) or (dia == “dom”)”);
resultado (falso);
sentença (“not (feriado == false)”);
resultado (verdadeiro);
sentença (“(dia == “seg”) or ! (feriado == false)”);
resultado (falso);

fimalgoritmo
